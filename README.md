# coded-error

Create error instances with a code, status, etc.

## Instalation

`$ npm install @burdzin/coded-error --save`

## Usage

```js
const CodedError = require('@burdzin/coded-error');
// there is a single error item by default:
// 0: {
//   message: 'Unknown error',
//   status: 500
// }

// we can create centralized list of all errors
CodedError.use({
  // override "Unknown error"
  0: {
    message: 'Internal error',
    status: 500
  },
  1000: {
    message: 'Authorization needed',
    status: 401
  },
  1001: {
    message: 'Missing parameter',
    status: 400
  },
  ...
});

// now we can throw error by code
throw new CodedError(1000);
// Error:
// - name: CodedError
// - code 1000
// - message: Authorization needed
// - status: 401

// we can override error fields
throw new CodedError(1000, {
  message: 'Authorization needed. 2 attempts left', // override
});
// Error:
// - name: CodedError
// - code 1000
// - message: Authorization needed. 2 attempts left
// - status: 401

// we can pass custom params
throw new CodedError(1000, {
  message: 'Authorization needed. 2 attempts left', // override
  details: { leftAttempts: 2 }, // pass some params
  quiet: true // pass other params
});
// Error:
// - name: CodedError
// - code 1000
// - message: Authorization needed. 2 attempts left
// - status: 401
// - details:
//   - leftAttempts: 2
// - quiet: true, (ok, we do not need to log the error)

// we can throw an error with non-existent code
throw new CodedError('NON_EXISTENT_CODE', { status: 504, message: 'Server do not respond' });
// Error:
// - name: CodedError
// - code NON_EXISTENT_CODE
// - message: Internal error -> Server do not respond
// - status: 500 -> 504
// - params: {}
// - meta: {}
```

## Tests

`$ npm test`

## License

MIT
