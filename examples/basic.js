const CodedError = require('../dist');
const express = require('express');

const NOT_FOUND = 'NOT_FOUND';
const SYS_ERROR = 'SYS_ERROR';
const ACCESS_DENIED = 'ACCESS_DENIED';

CodedError.use({
  [NOT_FOUND]: { status: 404, message: 'Not found' },
  [SYS_ERROR]: { status: 500, message: 'System error' },
  [ACCESS_DENIED]: { status: 403, message: 'Not today :)' }
});

const { NODE_ENV } = process.env;
const app = express();

app.all('/', (req, res, next) => {
  throw new CodedError(SYS_ERROR);
});

app.all('/error', (req, res, next) => {
  try {
    notExistingMethod();
  } catch (err) {
    next(err);
  }
});

app.all('/wrapped-error', (req, res, next) => {
  try {
    notExistingMethod();
  } catch (err) {
    next(new CodedError(SYS_ERROR).wrap(err));
  }
});

app.all('/secret', (req, res, next) => {
  next(new CodedError(ACCESS_DENIED));
});

app.use((req, res, next) => {
  const err = new CodedError(NOT_FOUND, { path: req.path });
  next(err);
});

app.use((err, req, res, next) => {
  const defaultCode = 500;
  const defaultData = CodedError.get(defaultCode);

  const {
    name,
    code = defaultData.code,
    message = defaultData.message,
    status = defaultData.status,
    quiet = false,
    stack,
    $wrapped,
    ...rest
  } = err;

  const output =
    NODE_ENV === 'development'
      ? {
          name,
          code,
          message,
          status,
          quiet,
          stack,
          $wrapped,
          ...rest
        }
      : {
          code,
          message,
          ...rest
        };

  res.status(status).json(output);

  if (!quiet) {
    console.error({
      name,
      code,
      message,
      status,
      quiet,
      stack,
      $wrapped,
      ...rest
    });
  }
});

app.listen(8080, () => console.log('> listening on 8080'));
