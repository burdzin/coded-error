const zeroCode = {
  0: {
    message: 'Unknown error',
    status: 500
  }
};

let codes = { ...zeroCode };

/**
 *
 * @param {number|string} code - Error code
 * @returns {object} - Error
 */
const get = (code) => (code in codes ? { ...codes[code], code } : { ...codes[0], code });

/**
 *
 * @param {object} input - Map error
 */
const use = (input) => {
  if (!(typeof input === 'object' && input.constructor === Object) || input == null) {
    throw new Error('input should be an object');
  }

  for (let key in input) {
    const { status, message } = input[key];

    if (status == null) {
      const message = `key '${key}' should to contain 'status' field`;
      throw new Error(message);
    }

    if (message == null) {
      const message = `key '${key}' should to contain 'message' field`;
      throw new Error(message);
    }
  }

  codes = { ...zeroCode, ...input };
};

export { get, use };
