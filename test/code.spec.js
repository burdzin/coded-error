const { expect, should } = require('chai');

const { codes } = require('../dist');

const errorCodes = {
  0: {
    message: 'Unknown',
    status: 500
  },
  1000: {
    message: 'Test',
    status: 400,
    optionalField: 'optionalField'
  },
  TEST_ERR: {
    message: 'Test',
    status: 401
  }
};

should();

describe('Codes', () => {
  describe('Codes.use()', () => {
    it('should throw an error', (done) => {
      expect(() => codes.use()).to.throw();
      expect(() => codes.use(null)).to.throw();
      expect(() => codes.use([])).to.throw();
      expect(() => codes.use({ 1: { messasge: 1 } })).to.throw();
      expect(() => codes.use({ 1: { status: 1 } })).to.throw();
      done();
    });

    it('should correct work', (done) => {
      expect(() => codes.use({})).to.not.throw();
      expect(() => codes.use(errorCodes)).to.not.throw();
      done();
    });
  });

  describe('Codes.get()', () => {
    before(() => codes.use(errorCodes));

    it('should return correct result', (done) => {
      const code = 1000;

      codes
        .get(code)
        .should.has.property('message')
        .equals(errorCodes[code].message);

      codes
        .get(code)
        .should.has.property('status')
        .equals(errorCodes[code].status);

      codes
        .get(code)
        .should.has.property('optionalField')
        .equals(errorCodes[code].optionalField);

      codes
        .get('TEST_ERR')
        .should.has.property('message')
        .equals(errorCodes['TEST_ERR'].message);

      codes
        .get('TEST_ERR')
        .should.has.property('message')
        .equals(errorCodes['TEST_ERR'].message);

      codes.get('TEST_ERR').should.not.has.property('optionalField');

      codes
        .get(5000)
        .should.has.property('message')
        .equals(errorCodes['0'].message);

      codes
        .get(5000)
        .should.has.property('status')
        .equals(errorCodes['0'].status);

      done();
    });
  });
});
