const chai = require('chai');

const CodedError = require('../dist');
const { codes } = require('../dist');

chai.should();

describe('CodedError.wrap()', () => {
  it('should wrap', (done) => {
    const code = 1000;
    const nativeError = new Error('test error');
    const codedError = new CodedError(code).wrap(nativeError);

    codedError.should.have.property('code').eql(code);
    codedError.should.have.property('$wrapped');
    codedError.$wrapped.should.have.property('name').eql(nativeError.name);
    codedError.$wrapped.should.have.property('stack').eql(nativeError.stack);
    codedError.$wrapped.should.have.property('message').eql(nativeError.message);

    done();
  });

  it('should not wrap', (done) => {
    const code = 1000;
    const codedError1 = new CodedError(code).wrap({});
    const codedError2 = new CodedError(code).wrap();

    codedError1.should.have.property('code');
    codedError1.should.not.have.property('$wrapped');

    codedError2.should.have.property('code');
    codedError2.should.not.have.property('$wrapped');

    done();
  });
});

describe('CodedError', () => {
  it('should return correct result', (done) => {
    const codedError = new CodedError();

    codedError.should.have.property('code').eql(0);
    codedError.should.have.property('message').eql(codes.get().message);
    codedError.should.have.property('status').eql(codes.get().status);

    done();
  });

  it('should return correct result by code', (done) => {
    const code = 1000;
    const codedError = new CodedError(code);

    codedError.should.have.property('code').eql(code);
    codedError.should.have.property('message').eql(codes.get(code).message);
    codedError.should.have.property('status').eql(codes.get(code).status);

    done();
  });

  it('should return correct result by message', (done) => {
    const code = 1000;
    const message = 'Test';
    const codedError = new CodedError(code, { message });

    codedError.should.have.property('code').eql(code);
    codedError.should.have.property('message').eql(message);
    codedError.should.have.property('status').eql(codes.get(code).status);

    done();
  });

  it('should return correct result by status', (done) => {
    const code = 1000;
    const status = 200;
    const codedError = new CodedError(code, { status });

    codedError.should.have.property('code').eql(code);
    codedError.should.have.property('message').eql(codes.get(code).message);
    codedError.should.have.property('status').eql(status);

    done();
  });

  it('should return optional params', (done) => {
    const code = 1000;
    const status = 200;
    const test = { nested: 1 };
    const codedError = new CodedError(code, { status, test });

    codedError.should.have.property('code').eql(code);
    codedError.should.have.property('message').eql(codes.get(code).message);
    codedError.should.have.property('status').eql(status);
    codedError.should.have.property('test').eql(test);

    done();
  });
});
